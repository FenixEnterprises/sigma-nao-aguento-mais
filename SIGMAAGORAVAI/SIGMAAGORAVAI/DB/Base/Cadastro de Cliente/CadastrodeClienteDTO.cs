﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMAAGORAVAI.DB.Base.Cadastro_de_Cliente
{
    class CadastrodeClienteDTO
    {
        public int ID { get; set; }

        public int Cliente { get; set; }

        public string Nome  { get; set; }

        public DateTime Data_Nascimento { get; set; }

        public string CEP{ get; set; }

        public string Endereço { get; set; }

        public string Cidade { get; set; }

        public string Bairro{ get; set; }

        public string Telefone{ get; set; }

        public string Celular { get; set; }
    }
}
