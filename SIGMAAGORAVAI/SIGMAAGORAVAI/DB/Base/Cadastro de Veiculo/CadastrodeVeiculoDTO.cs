﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMAAGORAVAI.DB.Base.Cadastro_de_Veiculo
{
    class CadastrodeVeiculoDTO
    {
        public int ID { get; set; }

        public int Veiculo { get; set; }

        public string Cor{ get; set; }

        public int Ano { get; set; }

        public string placa { get; set; }

        public string Marca { get; set; }

        public string Modelo { get; set; }
    }
}
