﻿namespace SIGMAAGORAVAI.DB.Telas
{
    partial class Localizar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdnCodigo = new System.Windows.Forms.RadioButton();
            this.rdnDescricao = new System.Windows.Forms.RadioButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblEsolha_o_tipo_de_busca = new System.Windows.Forms.Label();
            this.lblTipoDeBusca = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // rdnCodigo
            // 
            this.rdnCodigo.AutoSize = true;
            this.rdnCodigo.Location = new System.Drawing.Point(0, 17);
            this.rdnCodigo.Name = "rdnCodigo";
            this.rdnCodigo.Size = new System.Drawing.Size(58, 17);
            this.rdnCodigo.TabIndex = 0;
            this.rdnCodigo.TabStop = true;
            this.rdnCodigo.Text = "Codigo";
            this.rdnCodigo.UseVisualStyleBackColor = true;
            // 
            // rdnDescricao
            // 
            this.rdnDescricao.AutoSize = true;
            this.rdnDescricao.Location = new System.Drawing.Point(0, 40);
            this.rdnDescricao.Name = "rdnDescricao";
            this.rdnDescricao.Size = new System.Drawing.Size(73, 17);
            this.rdnDescricao.TabIndex = 1;
            this.rdnDescricao.TabStop = true;
            this.rdnDescricao.Text = "Descrição";
            this.rdnDescricao.UseVisualStyleBackColor = true;
            this.rdnDescricao.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.MintCream;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 63);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(452, 150);
            this.dataGridView1.TabIndex = 2;
            // 
            // lblEsolha_o_tipo_de_busca
            // 
            this.lblEsolha_o_tipo_de_busca.AutoSize = true;
            this.lblEsolha_o_tipo_de_busca.Location = new System.Drawing.Point(94, 9);
            this.lblEsolha_o_tipo_de_busca.Name = "lblEsolha_o_tipo_de_busca";
            this.lblEsolha_o_tipo_de_busca.Size = new System.Drawing.Size(274, 13);
            this.lblEsolha_o_tipo_de_busca.TabIndex = 3;
            this.lblEsolha_o_tipo_de_busca.Text = "Escolha o Tipo de Busca, digite o valor e aperte ENTER";
            this.lblEsolha_o_tipo_de_busca.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblTipoDeBusca
            // 
            this.lblTipoDeBusca.AutoSize = true;
            this.lblTipoDeBusca.Location = new System.Drawing.Point(-3, 0);
            this.lblTipoDeBusca.Name = "lblTipoDeBusca";
            this.lblTipoDeBusca.Size = new System.Drawing.Size(76, 13);
            this.lblTipoDeBusca.TabIndex = 4;
            this.lblTipoDeBusca.Text = "Tipo de Busca";
            this.lblTipoDeBusca.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(97, 37);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(343, 20);
            this.textBox1.TabIndex = 5;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnOk
            // 
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOk.Location = new System.Drawing.Point(199, 219);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(106, 42);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancelar.Location = new System.Drawing.Point(324, 219);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(116, 42);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // Localizar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 273);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblTipoDeBusca);
            this.Controls.Add(this.lblEsolha_o_tipo_de_busca);
            this.Controls.Add(this.rdnCodigo);
            this.Controls.Add(this.rdnDescricao);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Localizar";
            this.Text = "Localizar";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rdnCodigo;
        private System.Windows.Forms.RadioButton rdnDescricao;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblEsolha_o_tipo_de_busca;
        private System.Windows.Forms.Label lblTipoDeBusca;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancelar;
    }
}