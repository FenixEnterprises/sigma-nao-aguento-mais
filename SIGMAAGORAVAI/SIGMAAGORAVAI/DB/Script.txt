﻿CREATE DataBase Sigma;
USE `Sigma` ;

-- -----------------------------------------------------
-- Table `Centro_Automotivo`.`tb_funcionario`
-- -----------------------------------------------------
CREATE TABLE `tb_funcionario` (
  `id_funcionario` INT NOT NULL AUTO_INCREMENT,
  `usuario` VARCHAR(20) NOT NULL,
  `senha` VARCHAR(20) NOT NULL,
  `nome` VARCHAR(50) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  `CPF` VARCHAR(15) NOT NULL,
  `RG` VARCHAR(15) NOT NULL,
  `endereco` VARCHAR(50) NOT NULL,
  `complemento` VARCHAR(45) NULL,
  `cargo` VARCHAR(45) NULL,
  `observacao` VARCHAR(100) NULL,
  PRIMARY KEY (`id_funcionario`),
  UNIQUE INDEX `usuario_UNIQUE` (`usuario` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Centro_Automotivo`.`Atendimento`
-- -----------------------------------------------------
CREATE TABLE `Atendimento` (
  `id_atendimento` INT NOT NULL AUTO_INCREMENT,
  `data` DATE NULL,
  `descricao` TIME NULL,
  `situacao` VARCHAR(45) NOT NULL,
  `funcionario_id_funcionario` INT NULL,
  `orcamento_id_orcamento` INT NULL,
  `cliente_id_cliente` INT NULL,
  PRIMARY KEY (`id_atendimento`),
  INDEX `funcionario_id_funcionario_idx` (`funcionario_id_funcionario` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Centro_Automotivo`.`Orcamento`
-- -----------------------------------------------------
CREATE TABLE `Orcamento` (
  `id_Orcamento` INT NOT NULL,
  `date` DATE NOT NULL,
  `descricao` VARCHAR(300) NULL,
  `valor` DOUBLE NOT NULL,
  `situacao` VARCHAR(45) NOT NULL,
  `funcionario_id_funcionario` INT NULL,
  PRIMARY KEY (`id_Orcamento`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Centro_Automotivo`.`Pecas`
-- -----------------------------------------------------
CREATE TABLE `Pecas` (
  `id_pecas` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(50) NOT NULL,
  `quantidade` VARCHAR(50) NOT NULL,
  `valor` DOUBLE NULL,
  `descricao` VARCHAR(300) NULL,
  `orcamento_id_orcamento` INT NULL,
  PRIMARY KEY (`id_pecas`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Centro_Automotivo`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE `Cliente` (
  `id_cliente` INT NOT NULL AUTO_INCREMENT,
  `CPF_CNPJ` VARCHAR(20) NULL,
  `razao_social` VARCHAR(45) NULL,
  `rg` VARCHAR(12) NULL,
  `nome` VARCHAR(45) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  `CEP` VARCHAR(45) NULL,
  `Estado` VARCHAR(2) NOT NULL,
  `Cidade` VARCHAR(45) NOT NULL,
  `Bairro` VARCHAR(45) NOT NULL,
  `Endereco` VARCHAR(45) NOT NULL,
  `telefone` VARCHAR(45) NULL,
  `celular` VARCHAR(45) NULL,
  PRIMARY KEY (`id_cliente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Centro_Automotivo`.`Veiculos`
-- -----------------------------------------------------
CREATE TABLE `Veiculos` (
  `id_veiculo` INT NOT NULL AUTO_INCREMENT,
  `placa` VARCHAR(8) NOT NULL,
  `marca` VARCHAR(15) NOT NULL,
  `modelo` VARCHAR(15) NOT NULL,
  `cor` VARCHAR(15) NOT NULL,
  `cliente_id_cliente` INT NULL,
  PRIMARY KEY (`id_veiculo`),
  INDEX `cliente_id_cliente_idx` (`cliente_id_cliente` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
