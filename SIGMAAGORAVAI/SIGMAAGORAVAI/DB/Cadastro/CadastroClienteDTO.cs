﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMAAGORAVAI.DB.Cadastro
{
    class CadastroClienteDTO
    {
        public int ID { get; set; }

        public string Usuario { get; set; }

        public string Senha { get; set; }

        public string Nome { get; set; }

        public DateTime Date { get; set; }

        public string CPF { get; set; }

        public string RG { get; set; }

        public string Endereço { get; set; }

        public string complemento { get; set; }

        public string cargo { get; set; }

        public string observação { get; set; }
    }
}
