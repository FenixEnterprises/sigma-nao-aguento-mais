﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMAAGORAVAI.DB.Cadastro
{
    class CadastrodeClienteDatabase
    {
        public int Salvar(CadastroClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente(nm_Usuario,ds_Senha,nm_Nome,dt_Date,ds_CPF,ds_RG,ds_Endereco,ds_complemento,ds_cargo,ds_observacao)VALUES
                           (@nm_Usuario,@ds_Senha,@nm_Nome,@dt_Date,@ds_CPF,@ds_RG,@ds_ complemento,@ds_cargo,@ds_observacao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_Usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("nm_Nome", dto.Nome));
            parms.Add(new MySqlParameter("dt_Date", dto.Date));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_complemento", dto.complemento));
            parms.Add(new MySqlParameter("ds_cargo", dto.cargo));
            parms.Add(new MySqlParameter("ds_observacao", dto.observação));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPK(script, parms);

        }
       

    }
}
